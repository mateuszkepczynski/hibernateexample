import Util.HibernateUtil;
import domain.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.sql.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.Currency;

/**
 * Created by RENT on 2017-09-25.
 */
public class Application {
    public static void main(String[] args) {
        SessionFactory sessionFactory = HibernateUtil.getSessionFactory();
        Session session = sessionFactory.openSession();

        try {
            session.beginTransaction();

//            Address address = new Address();
//            address.setState("Pomorskie");
//            address.setStreet("Do studzienki");
//            address.setCity("Gdańsk");
//            session.persist(address);
//
//            Customer customer = new Customer();
//            customer.setName("Lechu");
//            customer.setLastName("Nowak");
//            customer.setAddress(address);
//            session.persist(customer);

            Brain brain = new Brain();
            brain.setSomething("what");
            session.persist(brain);

            Human human = new Human();
            human.setBrain(brain);
            human.setName("Karola");
            human.setLastName("Karola");
            human.setId(brain.getId());
            session.persist(human);
            session.getTransaction().commit();
        } catch (Throwable e) {
            session.getTransaction().rollback();
        } finally {
            session.close();
            sessionFactory.close();
        }
    }
}
