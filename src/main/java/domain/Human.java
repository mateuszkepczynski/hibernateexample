package domain;

import javax.persistence.*;

@Entity
public class Human {
    @Id
    private long id;
    private String name;
    private String lastName;
    @OneToOne
    @PrimaryKeyJoinColumn(name = "BRAIN_ID")
    private Brain brain;

    public Human() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Brain getBrain() {
        return brain;
    }

    public void setBrain(Brain brain) {
        this.brain = brain;
    }
}
