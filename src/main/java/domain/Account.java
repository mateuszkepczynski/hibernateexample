package domain;

import javax.persistence.*;
import java.util.Date;


@Entity
@Table(name = "Employee_Account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int ID;
    @Column(name = "name_account")
    private String nameAccount;
    @Column(name = "int_account")
    private int intAccount;
    @Column(precision = 5,name = "double_Account")
    private double doubleAccount;
    @Transient
    private String something;
    @Temporal(TemporalType.DATE)
    private Date data;
    @Temporal(TemporalType.TIME)
    private Date time;
    @Enumerated(EnumType.ORDINAL)
    private MyEnumType enumId;
    @Enumerated(EnumType.STRING)
    private MyEnumType enumString;

    public Account() {
    }

    public Account(String nameAccount, int intAccount, double doubleAccount, String something, Date data, Date time, MyEnumType enumId, MyEnumType enumString) {
        this.nameAccount = nameAccount;
        this.intAccount = intAccount;
        this.doubleAccount = doubleAccount;
        this.something = something;
        this.data = data;
        this.time = time;
        this.enumId = enumId;
        this.enumString = enumString;
    }

    public String getNameAccount() {
        return nameAccount;
    }

    public void setNameAccount(String nameAccount) {
        this.nameAccount = nameAccount;
    }

    public int getIntAccount() {
        return intAccount;
    }

    public void setIntAccount(int intAccount) {
        this.intAccount = intAccount;
    }

    public double getDoubleAccount() {
        return doubleAccount;
    }

    public void setDoubleAccount(double doubleAccount) {
        this.doubleAccount = doubleAccount;
    }

    public String getSomething() {
        return something;
    }

    public void setSomething(String something) {
        this.something = something;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public MyEnumType getEnumId() {
        return enumId;
    }

    public void setEnumId(MyEnumType enumId) {
        this.enumId = enumId;
    }

    public MyEnumType getEnumString() {
        return enumString;
    }

    public void setEnumString(MyEnumType enumString) {
        this.enumString = enumString;
    }
}
